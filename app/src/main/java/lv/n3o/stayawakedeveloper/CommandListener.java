package lv.n3o.stayawakedeveloper;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;

/**
 * Created by elviss on 14.30.6.
 */
public class CommandListener extends Service
{

	PowerManager.WakeLock lock;

	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}

	@Override
	public void onCreate()
	{
		super.onCreate();
		PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
		lock = pm.newWakeLock(
				PowerManager.SCREEN_DIM_WAKE_LOCK
				| PowerManager.ON_AFTER_RELEASE
				| PowerManager.ACQUIRE_CAUSES_WAKEUP,
				"TAG");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		if (intent != null)
		{
			if (intent.getBooleanExtra("lock", false))
			{
				startLock();
			}
			else
			{
				stopLock();
			}
		}

		return START_STICKY;
	}

	private void stopLock()
	{
		if (lock.isHeld())
		{
			lock.release();
		}
	}

	private void startLock()
	{
		if (!lock.isHeld())
		{
			lock.acquire();
		}
	}
}
