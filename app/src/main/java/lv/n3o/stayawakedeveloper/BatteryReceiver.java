package lv.n3o.stayawakedeveloper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;


/**
 * Created by elviss on 14.30.6.
 */
public class BatteryReceiver extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		boolean isCharging = intent.getAction().equals(Intent.ACTION_POWER_CONNECTED);

		IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		intent = context.registerReceiver(null, ifilter);

		boolean usbCharge = (intent != null ? intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1) : -1) == BatteryManager.BATTERY_PLUGGED_USB;


		boolean wakeLock = false;
		if (isCharging && usbCharge)
		{
			wakeLock = true;
		}

		Log.e("CHG", isCharging ? "ON" : "OFF");
		Log.e("USB", usbCharge ? "ON" : "OFF");

		Intent serviceIntent = new Intent(context, CommandListener.class);
		serviceIntent.putExtra("lock", wakeLock);
		context.startService(serviceIntent);

		Log.e("AAA", wakeLock ? "ON" : "OFF");

	}
}
